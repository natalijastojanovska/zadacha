namespace Zadaca{
    public interface IMessagePresenter{
        void EmptyList();
        void None();
        void Domestic();
        void Imported();
        void Name(string name);
        void Price(float price);
        void Description(string description);
        void Weight(float weight);
        void ImportedCost(float cost);
        void ImportedCount(int count);
        void DomesticCost(float cost);
        void DomesticCount(int count);

    }
}
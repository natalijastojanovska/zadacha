namespace Zadaca{
    public interface IProductReader{
       void ListDomesticProducts();
       void ListImportedProducts();
       void DomesticCost();
       void ImportedCost();
       void DomesticCount();
       void ImportedCount();
       bool EmptyList();
    }
}
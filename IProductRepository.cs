namespace Zadaca{
    public interface IProductRepository{
        List<Product> GetAllProducts();
    }
}
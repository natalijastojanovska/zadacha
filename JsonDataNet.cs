using System.Net;
using System.Text.Json;

namespace Zadaca{
    public class JsonDataNet : IProductRepository{
        public List<Product>  GetAllProducts(){
            var json = new WebClient().DownloadString("https://interview-task-api.mca.dev/qr-scanner-codes/alpha-qr-gFpwhsQ8fkY1");
            var result = JsonSerializer.Deserialize<List<Product>>(json);
            return result ?? new List<Product>();
        }
    }
}
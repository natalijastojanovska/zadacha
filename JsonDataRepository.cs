using System.Text.Json;

namespace Zadaca{
    public class JsonDataRepository : IProductRepository{
        const string fileName = "products.json";

        public List<Product> GetAllProducts(){
            var json = File.ReadAllText(fileName);
            var result = JsonSerializer.Deserialize<ProductsRoot>(json);
            return result?.Products ?? new List<Product>();
        }
    }
}
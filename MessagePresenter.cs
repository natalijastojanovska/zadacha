namespace Zadaca{
    public class MessagePresenter : IMessagePresenter{
        public void EmptyList(){
            Console.WriteLine("The list does not contain any product.");
        }
        public void None(){
            Console.WriteLine("  None");
        }
        public void Domestic(){
            Console.WriteLine("Domestic ");
        }
        public void Imported(){
            Console.WriteLine("Imported ");
        }
        public void Name(string name){
            Console.WriteLine("  " + name);
        }
        public void Price(float price){
            Console.WriteLine("   Price: $" + price);
        }
        public void Description(string description){
            Console.WriteLine("   " + description + "...");
        }
        public void Weight(float weight){
            if(weight == 0){
            Console.WriteLine("   Weight: N/A");
        }else
            Console.WriteLine("   Weight: " + weight + "g");
        }

        public void ImportedCost(float cost){
            Console.WriteLine("Imported Cost: $" + cost);
        }
        public void ImportedCount(int count){
            Console.WriteLine("Imported Count: " + count);
        }
        public void DomesticCost(float cost){
            Console.WriteLine("Domestic Cost: $" + cost);
        }
        public void DomesticCount(int count){
        Console.WriteLine("Domestic Count: " + count);
        }
    }
}
namespace Zadaca{
    public class Product{
        public string name {get; set;} = "";
        public bool domestic {get; set;}
        public float price {get; set;}
        public float weight {get; set;}
        public string? description {get; set;}
    }
}
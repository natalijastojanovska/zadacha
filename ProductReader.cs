using System.Collections.Generic;

namespace Zadaca{
    public class ProductReader : IProductReader{
        private List<Product> productList = new List<Product>();
        IMessagePresenter messagePresenter = new MessagePresenter();

        public ProductReader(){
            // this.productList = new JsonDataRepository().GetAllProducts(); //reads from local json file

            this.productList = new JsonDataNet().GetAllProducts(); // reads api endpoint
            productList.Sort((x, y) => string.Compare(x.name, y.name));
        }
        
        public bool EmptyList(){
            if(productList.Count > 0){
                return true;
            }
            return false;
        }

        public void ListDomesticProducts(){
             messagePresenter.Domestic();
             bool flag = false;
             foreach(Product temp in productList){
                if(temp.domestic){
                    messagePresenter.Name(temp.name);
                    messagePresenter.Price(temp.price);
                    messagePresenter.Description(DescriptionTruncate(temp.description));
                    messagePresenter.Weight(temp.weight);
                    flag = true;
                }
             }
             if(!flag){
                messagePresenter.None();
             }
        }
        public void ListImportedProducts(){
            messagePresenter.Imported();
            bool flag = false;
            foreach(Product temp in productList){
                if(!temp.domestic){
                    messagePresenter.Name(temp.name);
                    messagePresenter.Price(temp.price);
                    messagePresenter.Description(DescriptionTruncate(temp?.description));
                    messagePresenter.Weight(temp.weight);
                    flag = true;
                }
            }if(!flag){
                messagePresenter.None();
             }
        }
        
        public void DomesticCost(){
            float total = 0;
            foreach(Product temp in productList){
                if(temp.domestic){
                total += temp.price;
            }
            }
            messagePresenter.DomesticCost(total);
        }

        public void ImportedCost(){
         float total = 0;
            foreach(Product temp in productList){
                if(!temp.domestic){
                total += temp.price;
            }
            }
            messagePresenter.ImportedCost(total);
        }
       public void DomesticCount(){
        var counter = 0;
            foreach(Product temp in productList){
                if(temp.domestic){
                counter++;
            }
            }
            messagePresenter.DomesticCount(counter);
       }
       public void ImportedCount(){
         var counter = 0;
            foreach(Product temp in productList){
                if(!temp.domestic){
                counter++;
            }
            }
            messagePresenter.ImportedCount(counter);
       }

       public string DescriptionTruncate(string? desc){
        if(desc.Length > 10){
            return desc.Substring(0,10);
        }
        return desc;
       }
    }
}
using System.Text.Json.Serialization;

namespace Zadaca{
    public class ProductsRoot{
        [JsonPropertyName("products")]
        public List<Product>? Products{get; set;}
    }
}
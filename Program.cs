﻿namespace Zadaca
{
    class Program{
        static void Main(string[] args){
            ProductReader reader = new ProductReader();
            IMessagePresenter messagePresenter = new MessagePresenter();
            if(reader.EmptyList()){
                reader.ListDomesticProducts();
                reader.ListImportedProducts();
                reader.DomesticCost();
                reader.ImportedCost();
                reader.DomesticCount();
                reader.ImportedCount();
            }else{
                messagePresenter.EmptyList();
            }
            
        }
    }
}